$(function (){
  'use strict';

  var randomScalingFactor = function(){ return Math.round(Math.random()*100)};
  var barChartData = {
    datasets : [
      {
        label: 'fFn',
        backgroundColor : '#6d6e71',
        borderColor : 'rgba(220,220,220,0.8)',
        highlightFill: 'rgba(220,220,220,0.75)',
        highlightStroke: 'rgba(220,220,220,1)',
        data : [randomScalingFactor()]
      },
      {
        label: 'PartoSure',
        backgroundColor : '#77a23f',
        borderColor : 'rgba(147, 190, 91,0.8)',
        highlightFill : 'rgba(147, 190, 91,0.75)',
        highlightStroke : 'rgba(147, 190, 91,1)',
        data : [randomScalingFactor()]
      }
    ]
  }
  var stackBarChartData = {
    labels: ['fFn', 'PartoSure'],
    datasets: [
      {
        label: 'Net Impact (Reimbursement - Costs), Absolute Value',
        backgroundColor : '#6d6e71',
        borderColor : 'rgba(220,220,220,0.8)',
        highlightFill: 'rgba(220,220,220,0.75)',
        highlightStroke: 'rgba(220,220,220,1)',
        data : [randomScalingFactor(), randomScalingFactor()]
      },
      {
        label: 'Annual Cost of Tests',
        backgroundColor : '#77a23f',
        borderColor : 'rgba(147, 190, 91,0.8)',
        highlightFill : 'rgba(147, 190, 91,0.75)',
        highlightStroke : 'rgba(147, 190, 91,1)',
        data : [randomScalingFactor(),randomScalingFactor()]
      }
    ]
  };
  var ctx = document.getElementById('canvas-total-cost');
  var chart1 = new Chart(ctx, {
    type: 'bar',
    data: barChartData,
    options: {
      responsive: true,
      animate: false
    }
  });

  var ctx = document.getElementById('canvas-total-benefit');
  var chart2 = new Chart(ctx, {
    type: 'bar',
    data: stackBarChartData,
    options: {
      legend: {
        display: false
      },
      responsive: true,
      scales: {
        xAxes: [{
          stacked: true
        }],
        yAxes: [{
          stacked: true
        }]
      }
    },
    animate: false
  });


  $('#calc_net_impact').on('click', function(e) {
    $('.net_impact_content').addClass('show');
    console.log(chart2)
  });

  });
